package com.vit.mynote.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vit.mynote.R;
import com.vit.mynote.models.Note;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoteListAdapter extends FirebaseRecyclerAdapter<Note, NoteListAdapter.ViewHolder> {

    private Context mContext;
    private DatabaseReference mNotesDatabase;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public NoteListAdapter(Context mContext, @NonNull FirebaseRecyclerOptions<Note> options, DatabaseReference notesDatabase) {
        super(options);
        this.mNotesDatabase = notesDatabase;
        this.mContext = mContext;
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, int position, @NonNull Note model) {
        String noteId = getRef(position).getKey();

        mNotesDatabase.child(noteId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String title = dataSnapshot.child("title").getValue().toString();
                String content = dataSnapshot.child("content").getValue().toString();

                holder.mTextTitle.setText(title);
                holder.mTextContent.setText(content);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAlertDialog();
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    /**
     * show Alert Dialog when click item note
     */
    private void showAlertDialog() {
        try {
            CharSequence options[] = new CharSequence[]{"Edit", "Delete"};

            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    if (i == 0) {
                        editNote();
                    } else if (i == 1) {
                        deleteNote();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * delete note of list
     */
    private void deleteNote() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * edit note of list
     */
    private void editNote() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_note_title)
        TextView mTextTitle;
        @BindView(R.id.item_note_content)
        TextView mTextContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(itemView);
        }


    }
}
