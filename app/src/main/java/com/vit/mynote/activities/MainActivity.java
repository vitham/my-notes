package com.vit.mynote.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vit.mynote.R;
import com.vit.mynote.adapters.NoteListAdapter;
import com.vit.mynote.models.Note;
import com.vit.mynote.utils.AppConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN_ACTIVITY:";

    private FirebaseAuth mAuth;
    private DatabaseReference mNotesDatabase;
    private String mCurrentId;

//    @BindView(R.id.main_list_notes)
//    RecyclerView mRvNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initFirebase();

//        mRvNotes.setHasFixedSize(true);
//        mRvNotes.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initFirebase() {
        try {
            mAuth = FirebaseAuth.getInstance();
            mCurrentId = mAuth.getCurrentUser().getUid();
            mNotesDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentId).child("Notes");
            mNotesDatabase.keepSynced(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        checkSignedIn();

//        fetchDataFromFirebase();
    }

    private void fetchDataFromFirebase() {
        try {

            FirebaseRecyclerOptions<Note> options =
                    new FirebaseRecyclerOptions.Builder<Note>()
                            .setQuery(mNotesDatabase, Note.class)
                            .build();

            NoteListAdapter adapter = new NoteListAdapter(this, options, mNotesDatabase);
//            mRvNotes.setAdapter(adapter);

        } catch (Exception ex) {
            Log.e(TAG, "fetchDataFromFirebase: ", ex);
        }
    }

    @OnClick(R.id.main_image_add)
    void onClickAdd(View view) {
        try {
            Intent intent = new Intent(MainActivity.this, CreateNoteActivity.class);
            intent.putExtra(AppConfig.CURRENT_ID, mCurrentId);
            Toast.makeText(this, mCurrentId, Toast.LENGTH_SHORT).show();
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "onClickAdd: ", e);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.menu_sign_out) {
            mAuth.signOut();
            sendToStart();
        }
        return true;
    }

    /**
     * Check user is signed in
     */
    private void checkSignedIn() {
        try {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) sendToStart();
        } catch (Exception e) {
            Log.e(TAG, "checkSignedIn: ", e);
        }
    }

    /**
     * jump to Start Activity
     */
    private void sendToStart() {
        try {
            startActivity(new Intent(this, StartActivity.class));
            finish();
        } catch (Exception e) {
            Log.e(TAG, "sendToStart: ", e);
        }
    }


}
