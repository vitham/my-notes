package com.vit.mynote.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.vit.mynote.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private FirebaseAuth mAuth;
    private ProgressDialog mLoginDialog;

    @BindView(R.id.login_input_email)
    TextInputLayout mInputEmail;

    @BindView(R.id.login_input_password)
    TextInputLayout mInputPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        mLoginDialog = new ProgressDialog(this);

    }

    @OnClick(R.id.login_button_login)
    void onClickLogin() {
        String email = mInputEmail.getEditText().getText().toString();
        String password = mInputPassword.getEditText().getText().toString();

        if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
            mLoginDialog.setTitle(R.string.login_progress_title);
            mLoginDialog.setMessage("Please wait");
            mLoginDialog.setCanceledOnTouchOutside(false);
            mLoginDialog.show();

            loginUser(email, password);
        } else {
            Toast.makeText(this, R.string.check_input_empty, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * connect Firebase and login
     *
     * @param email
     * @param password
     */
    private void loginUser(String email, String password) {

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");

                            loginSuccess();
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            mLoginDialog.hide();
                            Toast.makeText(LoginActivity.this, R.string.signin_fail,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void loginSuccess() {
        mLoginDialog.dismiss();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*@OnClick(R.id.login_button_facebook)
    void onClickFacebook() {
        //TODO Conect Firebase login facebook
    }

    @OnClick(R.id.login_button_gmail)
    void onClickGmail() {
        //TODO Conect Firebase login gmail
    }*/
}
