package com.vit.mynote.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vit.mynote.R;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity {

    //=========================== FIRLDS ==============================

    private static final String TAG = "SignupActivity";

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ProgressDialog mSignDialog;

    //=========================== VIEWS ==============================

        @BindView(R.id.signup_input_email)
    TextInputLayout mInputEmail;

        @BindView(R.id.signup_input_password)
    TextInputLayout mInputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        mSignDialog = new ProgressDialog(this);
    }

    @OnClick(R.id.signup_button_signup)
    void onClickSignup() {
        String email = mInputEmail.getEditText().getText().toString();
        String password = mInputPassword.getEditText().getText().toString();

        if (!TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)) {
            mSignDialog.setTitle(R.string.signup_progress_title);
            mSignDialog.setMessage("Please wait");
            mSignDialog.setCanceledOnTouchOutside(false);
            mSignDialog.show();

            signupUser(email, password);
        } else {
            Toast.makeText(this, R.string.check_input_empty, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * connect Firebase and signup
     * @param email
     * @param password
     */
    private void signupUser(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");

                            FirebaseUser user = mAuth.getCurrentUser();
                            String uId = user.getUid();
                            mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uId).child("Notes");

                            HashMap<String, String> userMap = new HashMap<>();
                            userMap.put("id", "-");

                            mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    signupSuccess();
                                }
                            });

                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            mSignDialog.dismiss();
                            Toast.makeText(SignupActivity.this, R.string.signup_fail,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    /**
     * when sifnup successful, jump main activity
     */
    private void signupSuccess() {
        mSignDialog.dismiss();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
