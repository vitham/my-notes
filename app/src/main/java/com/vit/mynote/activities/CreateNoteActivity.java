package com.vit.mynote.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.vit.mynote.R;
import com.vit.mynote.utils.AppConfig;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateNoteActivity extends AppCompatActivity {

    private static final String TAG = "CREATE_NOTE_ACTIVITY: ";

    private String mCurrentId;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mRef;

    @BindView(R.id.create_input_title)
    TextInputLayout mInputTitle;

    @BindView(R.id.create_input_content)
    TextInputLayout mInputContent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
        ButterKnife.bind(this);

        mCurrentId = getIntent().getStringExtra(AppConfig.CURRENT_ID);
        initFirebase();

    }

    private void initFirebase() {
        mDatabase = FirebaseDatabase.getInstance();
        mRef = mDatabase.getReference("Users").child(mCurrentId).child("Notes");
    }

    private void editNote() {
        try {
            String title = mInputTitle.getEditText().getText().toString();
            String content = mInputContent.getEditText().getText().toString();

            if (!TextUtils.isEmpty(title)) {
                pushToFirebase(title, content);
                startActivity(new Intent(this, MainActivity.class));
            } else {
                Toast.makeText(this, "Please enter title", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(TAG, "editNote: ", e);
        }

    }

    /**
     * push note to Firebase
     * @param title
     * @param content
     */
    private void pushToFirebase(String title, String content) {
        try {
            DatabaseReference notePush = mRef.push();
            String noteId = notePush.getKey();

            Map noteMap = new HashMap();
            noteMap.put(noteId + "/" + "title", title);
            noteMap.put(noteId + "/" + "content", content);

            mRef.updateChildren(noteMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Log.d("NOTE_LOG: ", databaseError.getMessage().toString());
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_create_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.menu_add) {
            editNote();
        }
        return true;
    }


}
