package com.vit.mynote.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.vit.mynote.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.start_button_login)
    void onClickLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.start_button_signup)
    void onClickSignup() {
        startActivity(new Intent(this, SignupActivity.class));
    }


}
